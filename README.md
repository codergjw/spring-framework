# Spring 手写框架

## 项目分析文档

手写 Spring 框架的项目分析文档地址：[项目分析文档](https://blog.csdn.net/weixin_50999696/article/details/132031337)

## 介绍
Spring 的简易框架版本。IOC 全称为：Inversion of Control。控制反转的基本概念是：不用创建对象，但是需要描述创建对象的方式。AOP （Aspect Orient Programming）,直译过来就是 面向切面编程,AOP 是一种编程思想，是面向对象编程（OOP）的一种补充。

## 1、Spring IOC

**Spring IOC 概述？**

提到 IOC，初次接触的人可能会觉得非常高大上，觉得是一种很高深的技术，然而事实呢？事实是 IOC 其实仅仅只是一个 Map 集合而已。

IOC 全称为：Inversion of Control。控制反转的基本概念是：不用创建对象，但是需要描述创建对象的方式。

简单的说我们本来在代码中创建一个对象是通过 new 关键字，而使用了 Spring 之后，我们不在需要自己去 new 一个对象了，而是直接通过容器里面去取出来，再将其自动注入到我们需要的对象之中，即：依赖注入。

也就说创建对象的控制权不在我们程序员手上了，全部交由 Spring 进行管理，程序要只需要注入就可以了，所以才称之为控制反转。

实际上，IOC 也被称之为 IOC 容器，那么既然是一个容器，肯定是要用来放东西的，那么 IOC 容器用来存储什么呢？如果大家对 Spring 有所了解的话，那就知道在 Spring 里面可以说是一切面向 Bean 编程，而 Bean 指的就是我们交给 Spring 管理的对象，今天我们要学习的 IOC 容器就是用来存储所有 Bean 的一个容器。


**Spring IOC 三大核心接口？**

1. **BeanDefinition**：定义了一个 Bean 相关配置文件的各种信息，比如当前 Bean 的构造器参数，属性，以及其他一些信息，这个接口同样也会衍生出其他一些实现类，如
2. **BeanDefinitionReader**：定义了一些读取配置文件的方法，支持使用 Resource 和 String 位置参数指定加载方法，具体的时候可以扩展自己的特有方法。该类只是提供了一个建议标准，不要求所有的解析都实现这个接口。
3. **BeanFactory**：访问 Bean 容器的顶层接口，我们最常用的 ApplicationContext 接口也实现了 BeanFactory。

![输入图片说明](image.png)

![输入图片说明](image2.png)

## 2、Spring AOP

### 2.1、SpringAOP 流程

#### 2.1.1、Aop 自动代理时机

在service bean的创建过程中(也就是getBean("service"))，AOP通过BeanPostProcess后置处理器操作进行介入 分为2种情况：

- 用户自定义了targetSource，则bean的创建(实例化、填充、初始化)均由用户负责，Spring Ioc不会在管该代理目标对象traget，这种情况基本上不会发生，很多人用了几年Spring可能都不知道有它的存在。
- 正常情况下都是Spring Ioc完成代理对象target的实例化、填充、初始化。然后在初始化后置处理器中进行介入，对bean也就是service进行代理。

![输入图片说明](image2.png)

![输入图片说明](image3.png)

![输入图片说明](image4.png)

注意：由于文档内容过多，需要的朋友可以加主页的联系方式

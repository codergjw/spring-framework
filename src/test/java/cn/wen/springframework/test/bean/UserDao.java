package cn.wen.springframework.test.bean;


import cn.wen.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class UserDao {

    private static Map<String, String> hashMap = new HashMap<>();

    static {
        hashMap.put("10001", "小飞，北京，亦庄");
        hashMap.put("10002", "飞飞，上海，尖沙咀");
        hashMap.put("10003", "阿飞，香港，铜锣湾");
    }

    public String queryUserName(String id) {
        return hashMap.get(id);
    }

}

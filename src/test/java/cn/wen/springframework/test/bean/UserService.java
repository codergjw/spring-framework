package cn.wen.springframework.test.bean;


import cn.wen.springframework.beans.factory.annotation.Autowired;
import cn.wen.springframework.beans.factory.annotation.Value;
import cn.wen.springframework.stereotype.Component;

import java.util.Random;

public class UserService implements IUserService {

    private String token;

    public String queryUserInfo() {
        try {
            Thread.sleep(new Random(1).nextInt(100));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "小飞飞，100001，深圳，" + token;
    }

    public String register(String userName) {
        try {
            Thread.sleep(new Random(1).nextInt(100));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "注册用户：" + userName + " success！";
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
